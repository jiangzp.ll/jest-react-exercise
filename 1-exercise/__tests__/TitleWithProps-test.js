// eslint-disable-next-line no-unused-vars
import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';

// <--start
// TODO: 少什么就引点儿什么吧。
import TitleWithProps from '../TitleWithProps';
// --end->

test('TitleWithProps组件渲染内容', () => {
  // <--start
  // TODO 1: 给出正确的assertion，测试Title组件渲染内容
  const actual = <TitleWithProps name="world" />;
  const renderResult = render(actual);

  expect(renderResult.queryByText('Hello world')).not.toBeNull();
  // --end->
});
